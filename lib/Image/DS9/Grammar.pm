package Image::DS9::Grammar;

# ABSTRACT: Grammar definitions
use v5.10;
use strict;
use warnings;

our $VERSION = 'v1.0.1';

use Safe::Isa;

use Image::DS9::Util 'is_TODO';
use Image::DS9::Grammar::V8_5 '_grammar';
use Exporter::Shiny 'grammar';

# COPYRIGHT

sub grammar {
    my $entry = _grammar( @_ );
    # $entry may be the entire grammar or just the entry for a single
    # command.  Either way, the following statement works.
    return is_TODO( $entry ) ? undef : $entry;
}

1;
