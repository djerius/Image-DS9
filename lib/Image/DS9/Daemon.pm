package Image::DS9::Daemon;

# ABSTRACT: Wrapper around Proc::Daemon to implement terminate_on_destroy

use v5.10;
use strict;
use warnings;

our $VERSION = 'v1.0.1';

use parent 'Proc::Daemon';

sub Init {
    my $self = shift;

    my @pid = $self->SUPER::Init( @_ );
    $self->{ +__PACKAGE__ }{pids} = \@pid;
    ## no critic (Community::Wantarray)
    # duplicate API from parent
    return ( wantarray ? @pid : $pid[0] );
}

sub alive {
    my $self = shift;
    my @pid  = @{ $self->{ +__PACKAGE__ }{pids} };

    my @dead = grep { !$self->SUPER::Status( $_ ) } @pid;
    return !scalar @dead;
}

sub DESTROY {
    my $self = shift;
    return unless $self->{terminate_on_destroy};

    my $pid = $self->{ +__PACKAGE__ }{pids};
    $self->Kill_Daemon( $_ ) for @{$pid};
}


1;

