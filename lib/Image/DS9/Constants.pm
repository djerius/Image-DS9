package Image::DS9::Constants;

# ABSTRACT:  predefined constants

use v5.10;
use strict;
use warnings;

require Image::DS9::Constants::V0;

our $VERSION = 'v1.0.1';

sub import {
    goto &Image::DS9::Constants::V0::import;
}


# COPYRIGHT

1;


__END__

=head1 SYNOPSIS

  # import all of the constants
  use Image::DS9::Constants;

=head1 DESCRIPTION

This module is now a front end to L<Image::DS9::Constants::V0>. It
eventually will be a front end to L<Image::DS9::Constants::V1>, as the
L<Image::DS9::Constants::V0/V0> version is now deprecated.

Please migrate your code to L<Image::DS9::Constants::V1>.

=cut
