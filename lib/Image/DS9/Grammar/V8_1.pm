package Image::DS9::Grammar::V8_1;

# ABSTRACT: Grammar definitions

use v5.10;
use strict;
use warnings;

our $VERSION = 'v1.0.0';

use Exporter::Shiny 'grammar';

use Image::DS9::PConsts;

use constant REGIONFORMAT => ENUM( qw( ds9 ciao saotng saoimage pros xy ) );

# TODO:
#  shm

## no critic (ValuesAndExpressions::ProhibitCommaSeparatedStatements)

my %Grammar = (

    #------------------------------------------------------

    # 3d []
    #     [view <az> <el>]
    #     [az <az>]
    #     [el <el>]
    #     [scale <scale>]
    #     [method mip|aip]
    #     [background none|azimuth|elevation]
    #     [border yes|no]
    #     [border color <color>]
    #     [highlite yes|no]
    #     [highlite color <color>]
    #     [compass yes|no]
    #     [compass color <color>]
    #     [match]
    #     [lock [yes|no]]
    #     [open|close]


    '3d' => [

        # view <az> <el>
        {
            cmd  => 'view',
            args => [
                az => FLOAT,
                el => FLOAT
            ],
            query => QYES,
        },

        # az <az>
        {
            cmd  => 'az',
            args => [
                az => FLOAT,
            ],
            query => QYES,
        },

        # el <el>
        {
            cmd  => 'el',
            args => [
                el => FLOAT,
            ],
            query => QYES,
        },

        #  scale <scale>
        {
            cmd  => 'scale',
            args => [
                scale => FLOAT,
            ],
            query => QYES,
        },

        #  method mip|aip
        {
            cmd  => 'method',
            args => [
                value => ENUM( 'mip', 'aip' ),
            ],
            query => QYES,
        },

        #  background none|azimuth|elevation
        {
            cmd  => 'background',
            args => [
                value => ENUM( 'none', 'azimuth', 'elevation' ),
            ],
            query => QYES,
        },

        #  border color <color>
        {
            cmd   => [qw( border color )],
            args  => [ color => STRING ],
            query => QYES,
        },

        #  border yes|no
        {
            cmd   => 'border',
            args  => [ value => BOOL ],
            query => QYES,
        },

        #  highlite color <color>
        {
            cmd  => [qw( highlite color)],
            args => [
                color => STRING
            ],
            query => QYES,
        },

        #  highlite yes|no
        {
            cmd   => 'highlite',
            args  => [ value => BOOL ],
            query => QYES,
        },

        #  compass color <color>
        {
            cmd  => [qw( compass color)],
            args => [
                color => STRING
            ],
            query => QYES,
        },

        #  compass yes|no
        {
            cmd   => 'compass',
            args  => [ value => BOOL ],
            query => QYES,
        },

        #  match
        {
            cmd   => 'match',
            query => QNONE
        },

        #  lock [yes|no]
        {
            cmd  => 'lock',
            args => [
                value => BOOL
            ],
            query => QYES
        },

        #  open|close
        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE,
        },

        #  []
        {
            query => QNONE
        },
    ],

    #------------------------------------------------------

    # about []

    about => [

        #  []
        {
            query => QONLY
        }
    ],

    #------------------------------------------------------

    # align []
    #     [yes|no]

    align => [

        # []
        # yes | no
        {
            args  => [ value => BOOL ],
            query => QYES
        }
    ],

    #------------------------------------------------------

    # analysis [<task number>]
    #     [<filename>]
    #     [task <task number>|<task name>]
    #     [load <filename>]
    #     [clear]
    #     [clear][load <filename>]
    #     [message ok|okcancel|yesno <message>]
    #     [entry <message>]
    #     [text]

    analysis => [

        #  task <task number>
        {
            cmd   => 'task',
            args  => [ 'task number or task name' => STRING ],
            query => QYES,
        },

        #  load <filename>
        {
            cmd   => 'load',
            args  => [ filename => STRING ],
            query => QNONE,
        },

        #  load < buffer
        {
            cmd       => 'load',
            query     => QNONE,
            bufferarg => 1,
        },

        # clear load <filename>
        {
            cmd   => [qw( clear load )],
            args  => [ filename => STRING ],
            query => QNONE,
        },

        # clear
        {
            cmd   => 'clear',
            query => QNONE,
        },

        # message ok|okcancel|yesno <message>
        {
            cmd  => 'message',
            args => [
                type    => ENUM( 'ok', 'okcancel', 'yesno' ),
                message => STRING,
            ],
            query => QONLY,
        },

        # ???: example shows this but syntax doesn' t have this .
        # message
        {
            cmd  => 'message',
            args => [
                message => STRING
            ],
            query => QNONE,
        },

        # entry <message>
        {
            cmd  => 'entry',
            args => [
                message => STRING,
            ],
            query => QONLY,
        },

        # text <message>
        {
            cmd  => 'text',
            args => [
                message => STRING,
            ],
            query => QNONE,
        },

        # text
        {
            cmd     => 'text',
            query   => QNONE,
            buffarg => 1
        },

        # ???: what does this return?
        # []
        {
            query => QONLY,
        },
    ],


    #------------------------------------------------------

    # array [native|little|big]
    # array [new|mask] [[xdim=<x>,ydim=<y>|dim=<dim>],zdim=<z>,bitpix=<b>,skip=<s>,endian=[little|big]]

    array => [

        # [native|little|big]
        {
            args   => [ OPTIONAL( endian => ENDIAN ) ],
            query  => QONLY,
            bufarg => 1
        },

        # [new|mask] < PDL
        {
            args   => [ array => PDL ],
            attrs  => [ -o    => [ ( new => BOOL ), ( mask => BOOL ) ], ],
            query  => QNONE,
            bufarg => 1
        },

        # [new|mask]  [xdim=<x>,ydim=<y>|dim=<dim>] zdim=<z> bitpix=<b> skip=<s> endian=[little|big]
        # < RAW
        {
            args  => [ array => SCALARREF ],
            attrs => [
                -o     => [ ( new => BOOL ), ( mask => BOOL ) ],
                bitpix => INT,
                skip   => POSINT,
                endian => ENDIAN,
                -o     => [ ( -a => [ xdim => POSINT, ydim => POSINT ] ), ( dim => POSINT ) ],
            ],
            query  => QNONE,
            bufarg => 1
        },

        # [new|mask] <file> [xdim=<x>,ydim=<y>|dim=<dim>] zdim=<z> bitpix=<b> skip=<s> endian=[little|big]
        {
            args  => [ filename => STRING ],
            attrs => [
                -o     => [ ( new => BOOL ), ( mask => BOOL ) ],
                bitpix => INT,
                skip   => POSINT,
                endian => ENDIAN,
                -o     => [ ( -a => [ xdim => POSINT, ydim => POSINT ] ), ( dim => POSINT ) ],
            ],
            query => QNONE,
        },

    ],

    #------------------------------------------------------

    # bg <color>
    # background

    ENUM( 'bg', 'background' ) => [ {
            args  => [ color => STRING ],
            query => QYES,
        },
    ],

    #------------------------------------------------------

    # backup <filename>
    backup => [ {
            args  => [ filename => STRING ],
            query => QNONE
        }
    ],

    #------------------------------------------------------

    # bin [about <x> <y>]
    #     [about center]
    #     [buffersize <value>]
    #     [cols <x> <y>]
    #     [colsz <x> <y> <z>]
    #     [factor <value> [<vector>]]
    #     [depth <value>]
    #     [filter <string>]
    #     [filter clear]
    #     [function average|sum]
    #     [in]
    #     [out]
    #     [to fit]
    #     [match]
    #     [lock [yes|no]]
    #     [open|close]

    bin => [

        # about center
        {
            cmd   => [qw( about center )],
            query => QNONE,
        },

        # about <x> <y>
        {
            cmd   => 'about',
            args  => [ x => FLOAT, y => FLOAT ],
            query => QYES,
        },

        # buffersize <size>
        {
            cmd   => 'buffersize',
            args  => [ size => POSINT ],
            query => QYES,
        },

        # cols <x> <y>
        {
            cmd   => 'cols',
            args  => [ x => STRING, y => STRING, ],
            rvals => [ARRAY],
            query => QYES,
        },

        # colsz <x> <y> <z>
        {
            cmd   => 'colsz',
            args  => [ x => STRING, y => STRING, z => STRING, ],
            rvals => [ARRAY],
            query => QNONE,
        },

        # factor <value> [<vector]
        {
            cmd  => 'factor',
            args => [
                value => FLOAT,
                OPTIONAL( vector => FLOAT ),
            ],
            rvals => [ARRAY],
            query => QYES,
        },

        # depth <value>
        {
            cmd   => 'depth',
            args  => [ value => POSINT ],
            query => QYES,
        },

        # filter clear
        {
            cmd   => [ 'filter', 'clear' ],
            query => QNONE,
        },

        # filter <string>
        {
            cmd   => 'filter',
            args  => [ filter => STRING_STRIP ],
            query => QYES,
        },

        # function average|sum
        {
            cmd   => 'function',
            args  => [ function => ENUM( 'average', 'sum' ) ],
            query => QYES,
        },

        # in
        {
            cmd   => 'in',
            query => QNONE
        },

        # out
        {
            cmd   => 'out',
            query => QNONE
        },

        # to fit
        {
            cmd   => 'to fit',
            match => qr/^to fit|tofit$/,
            query => QNONE,
        },

        # match
        {
            cmd   => 'match',
            query => QNONE,
        },

        # lock [yes|no\
        {
            cmd   => 'lock',
            args  => [ value => BOOL ],
            query => QYES,
        },

        # open|close
        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE,
        },

        # smooth function
        {
            cmd   => [ 'smooth', 'function' ],
            args  => [ function => STRING ],
            query => QONLY,
        },

        # smooth radius
        {
            cmd   => [ 'smooth', 'radius' ],
            args  => [ radius => FLOAT ],
            query => QONLY,
        },

        # unknown return values
        {
            cmd   => ['smooth'],
            query => QONLY,
        },

    ],

    #------------------------------------------------------

    # blink []
    #    [yes|no]
    #    [interval <value>]

    blink => [

        # interval <value>
        {
            cmd   => 'interval',
            args  => [ time => FLOAT ],
            query => QYES,
        },

        # yes|no
        {
            args  => [ state => BOOL ],
            query => QNONE,
        },

        # ???: unknown return values
        # []
        {
            query => QYES,
        }
    ],


    #------------------------------------------------------

    # block[<value>]
    #    [<value> <value>]
    #    [to <value>]
    #    [to <value><value>]
    #    [in]
    #    [out]
    #    [to fit]
    #    [match]
    #    [lock [yes|no]]
    #    [open|close]

    block => [

        #    [to fit]
        {
            cmd   => [ 'to', 'fit' ],
            query => QNONE,
        },

        #    [to <value> <value>]
        {
            cmd   => 'to',
            args  => [ x => FLOAT, y => FLOAT ],
            query => QNONE,
        },

        #    [to <value>]
        {
            cmd   => 'to',
            args  => [ xy => FLOAT ],
            query => QNONE,
        },

        #    [in]
        #    [out]
        #    [match]
        {
            cmd   => ENUM( 'in', 'out', 'match' ),
            query => QNONE,
        },

        #    [lock [yes|no]]
        {
            cmd   => 'lock',
            args  => [ value => BOOL ],
            query => QYES,
        },

        #    [open|close]
        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE
        },

        #    [<value> <value>]
        {
            args  => [ x => INT, y => INT ],
            query => QNONE,
        },

        # [<value>]
        {
            args  => [ xy => INT ],
            query => QNONE,
        },

        {
            rvals => [ARRAY],
            query => QYES,
        },
    ],

    #------------------------------------------------------

    # catalog [new]
    #    [ned|simbad|denis|skybot]
    #    [aavso|ac|ascss|cmc|gaia|gsc1|gsc2|gsc3|nomad|ppmx|sao|sdss5|sdss6|sdss7|sdss8|sdss9|tycho]
    #    [ua2|ub1|ucac2|ucac2sup|ucac3|ucac4|urat1]
    #    [2mass|iras]
    #    [csc|xmm|rosat]
    #    [first|nvss]
    #    [chandralog|cfhtlog|esolog|stlog|xmmlog]
    #    [cds <catalogname>]
    #    [cds <catalogid>]

    #    [load <filename>]
    #    [import sb|tsv <filename>]

#    [<ref>] [<ra> <dec> <coordsys>]
#    [<ref>] [allcols]
#    [<ref>] [allrows]
#    [<ref>] [cancel]
#    [<ref>] [clear]
#    [<ref>] [close]
#    [<ref>] [crosshair]
#    [<ref>] [dec <col>]
#    [<ref>] [edit yes|no]
#    [<ref>] [export sb|tsv <filename>]
#    [<ref>] [filter <string>]
#    [<ref>] [filter load <filename>]
#    [<ref>] [header]
#    [<ref>] [location <code>]
#    [<ref>] [match]
#    [<ref>] [match <ref> <ref>]
#    [<ref>] [match error <value> degrees|arcmin|arcsec]
#    [<ref>] [match function 1and2|1not2|2not1]
#    [<ref>] [match return 1and2|1only|2only]
#    [<ref>] [match unique yes|no]
#    [<ref>] [maxrows <number>]
#    [<ref>] [name <object>]
#    [<ref>] [panto yes|no]
#    [<ref>] [plot <xcol> <ycol> <xerrcol> <yerrcol>]
#    [<ref>] [print]
#    [<ref>] [psky <skyframe>]
#    [<ref>] [psystem <coordsys>]
#    [<ref>] [ra <col>]
#    [<ref>] [regions]
#    [<ref>] [retrieve]
#    [<ref>] [samp]
#    [<ref>] [samp broadcast]
#    [<ref>] [samp send <application>]
#    [<ref>] [save <filename>]
#    [<ref>] [server cds|adac|cadc|cambridge|sao|ukirt|iucaa|bejing|saao]
#    [<ref>] [show yes|no]
#    [<ref>] [radius <value> degrees|arcmin|arcsec]
#    [<ref>] [sky <skyframe>]
#    [<ref>] [skyformat <skyformat>]
#    [<ref>] [sort <col> incr|decr]
#    [<ref>] [symbol [#] condition|shape|color|text|font|fontsize|fontweight|fontslant <value>]
#    [<ref>] [symbol [#] text|size|size2|units|angle <value>]
#    [<ref>] [symbol shape circle|ellipse|box|text|point]
#    [<ref>] [symbol shapecircle point|box point|diamond point|cross point|x point|arrow point|boxcircle point]
#    [<ref>] [symbol add| [#] remove]
#    [<ref>] [symbol save|load <filename>]
#    [<ref>] [system <coordsys>]
#    [<ref>] [update]
#    [<ref>] [x <col>]
#    [<ref>] [y <col>]

    catalog => [

        do {

            my @catalogues = qw[
              ned simbad denis skybot
              aavso ac ascss cmc gaia gsc1 gsc2 gsc3 nomad ppmx sao sdss5 sdss6 sdss7 sdss8 sdss9 tycho
              ua2 ub1 ucac2 ucac2sup ucac3 ucac4 urat1
              2mass iras
              csc xmm rosat
              first nvss
              chandralog cfhtlog esolog stlog xmmlog
            ];

            {
                cmd   => ENUM( 'new', @catalogues ),
                query => QNONE,
            },

              {
                cmd   => 'cds',
                args  => [ catalog_name => ENUM( 'new', @catalogues ) ],
                query => QNONE,
              },

              {
                cmd   => 'cds',
                args  => [ catalog_id => STRING ],
                query => QNONE,
              },


              {
                cmd   => 'load',
                args  => [ filename => STRING ],
                query => QNONE,
              },

              {
                cmd   => [ ENUM( 'export', 'import' ), ENUM( 'sb', 'tsv' ) ],
                args  => [ filename => STRING ],
                query => QNONE,
              },

              # combine a bunch of similar no arg commands with no queries
              {
                cmd   => ENUM( 'cancel', 'clear', 'close', ),
                query => QNONE,
              },

              # combine a bunch of similar no arg commands with queries
              {
                cmd   => ENUM( 'allcols', 'allrows', 'crosshair', 'header' ),
                query => QYES,
              },

              {
                cmd   => 'dec',
                args  => [ column => STRING ],
                query => QYES,
              },

              {
                cmd   => [ 'filter', 'load' ],
                arg   => [ filename => STRING ],
                query => QNONE,
              },

              {
                cmd   => 'filter',
                arg   => [ filter => STRING ],
                query => QNONE,
              },

              {
                cmd   => 'location',
                arg   => [ code => STRING ],
                query => QNONE,
              },

              {
                cmd   => 'edit',
                args  => [ value => BOOL ],
                query => QYES,
              },

              {
                cmd  => [ 'match', 'error' ],
                args => [
                    error => FLOAT,
                    units => ANGLE_UNIT,
                ],
                query => QYES,
              },

              {
                cmd  => [ 'match', ENUM( 'function', 'return' ) ],
                args => [
                    value => ENUM( '1and2', '1not2', '2not1' ),
                ],
                query => QYES,
              },

              {
                cmd   => [ 'match', 'unique' ],
                args  => [ value => BOOL ],
                query => QYES,
              },

              # [<ref>] [match <ref> <ref>]
              {
                cmd   => 'maxrows',
                args  => [ number => POSINT ],
                query => QYES,
              },

              {
                cmd   => 'name',
                args  => [ object => STRING ],
                query => QYES,
              },

              {
                cmd   => 'panto',
                args  => [ value => BOOL ],
                query => QYES,
              },

              {
                cmd  => 'plot',
                args => [
                    xcol => STRING,
                    ycol => STRING,
                    xerr => STRING,
                    yerr => STRING,
                ],
                query => QYES,
              },

              {
                cmd   => 'print',
                query => QNONE,
              },

              {
                cmd   => 'psky',
                args  => [ skyframe => STRING, ],
                query => QYES,
              },

              {
                cmd   => 'psystem',
                args  => [ coordsys => COORDSYS, ],
                query => QYES,
              },

              {
                cmd   => 'ra',
                args  => [ column => STRING ],
                query => QNONE,
              },


              {
                cmd   => 'regions',
                query => QNONE,
              },

              {
                cmd   => 'retrieve',
                query => QNONE,
              },
              {
                cmd   => 'samp',
                query => QNONE,
              },
              {
                cmd   => [ 'samp', 'broadcast' ],
                query => QNONE,
              },
              {
                cmd   => [ 'samp', 'send' ],
                args  => [ application => STRING ],
                query => QNONE,
              },
              {
                cmd   => 'save',
                args  => [ filename => STRING ],
                query => QNONE,
              },
              {
                cmd  => 'server',
                args => [
                    server => ENUM( 'cds', 'adac', 'cadc', 'cambridge', 'sao', 'ukirt', 'iucaa', 'bejing', 'saao ' )
                ],
                query => QYES,
              },
              {
                cmd   => 'show',
                args  => [ value => BOOL ],
                query => QYES,
              },
              {
                cmd  => 'radius',
                args => [
                    value => FLOAT,
                    units => ANGLE_UNIT,
                ],
                query => QYES,
              },
              {
                cmd   => 'sky',
                args  => [ skyframe => SKYFRAME ],
                query => QYES,
              },
              {
                cmd   => 'skyformat',
                args  => [ skyformat => SKYFORMAT ],
                query => QYES,
              },
              {
                cmd  => 'sort',
                args => [
                    column    => STRING,
                    direction => ENUM( 'incr', 'decr' ),
                ],
                query => QYES,
              },
              {
                cmd   => 'symbol',
                args  => ['[#] condition|shape|color|text|font|fontsize|fontweight|fontslant <value>'],
                query => QYES,
              },
              {
                cmd   => 'symbol',
                args  => ['[#] text|size|size2|units|angle <value>'],
                query => QYES,
              },
              {
                cmd  => [ 'symbol', 'shape' ],
                args => [
                    shape => ENUM( 'circle', 'ellipse', 'box', 'text', 'point' )
                ],
                query => QYES,
              },
              {
                cmd  => [ 'symbol', 'shapecircle' ],
                args => [
                    shapecircle => ENUM(
                        'point',
                        'box point',
                        'diamond point',
                        'cross point',
                        'x point',
                        'arrow point',
                        'boxcircle point'
                    )
                ],
                query => QYES,
              },
              {
                cmd   => 'symbol',
                args  => ['add| [#] remove'],
                query => QNONE,
              },
              {
                cmd   => [ 'symbol', ENUM( 'save', 'load' ) ],
                args  => [ filename => STRING ],
                query => QNONE,
              },
              {
                cmd   => 'system',
                args  => [ coordsys => COORDSYS ],
                query => QYES,
              },
              {
                cmd   => 'update',
                query => QNONE,
              },
              {
                cmd   => ENUM( 'x', 'y' ),
                args  => [ column => STRING ],
                query => QYES,
              },
              {
                args  => [ ra => COORD_RA, dec => COORD_DEC ],
                query => QNONE,
              },
              ;
        },

    ],


    #------------------------------------------------------

    # cd [<directory>]

    cd => {
        args  => [ directory => STRING ],
        QUERY => QYES,
    },


    #------------------------------------------------------

    # cmap [<colormap>]
    #     [file]
    #     [load <filename>]
    #     [save <filename>]
    #     [invert yes|no]
    #     [<constrast> <bias>]
    #     [tag [load|save] <filename>]
    #     [tag delete]
    #     [open|close]

    cmap => [ {
            cmd   => 'file',
            args  => [ filename => STRING ],
            query => QONLY,
        },

        {
            cmd   => ENUM( 'load', 'save' ),
            args  => [ filename => STRING ],
            query => QNONE,
        },

        {
            cmd   => 'invert',
            args  => [ value => BOOL ],
            query => QYES,
        },

        {
            cmd   => [ tag      => ENUM( 'load', 'save' ) ],
            args  => [ filename => STRING ],
            query => QNONE,
        },

        {
            cmd   => [ 'tag', 'delete' ],
            query => QNONE,
        },

        {
            args  => [ contrast => FLOAT, bias => FLOAT ],
            query => QYES,

        },

        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE
        },

    ],

    #------------------------------------------------------

    # colorbar []
    #     [yes|no]
    #     [orientation]
    #     [horizontal|vertical]
    #     [numerics yes|no]
    #     [space value|distance]
    #     [font times|helvetica|courier]
    #     [fontsize <value>]
    #     [fontweight normal|bold]
    #     [fontslant roman|italic]
    #     [size]
    #     [ticks]
    #     [match]
    #     [lock [yes|no]]

    colorbar => [

        {
            cmd   => 'orientation',
            args  => [ orientation => ENUM( 'horizontal', 'vertical' ) ],
            query => QYES,
        },
        {
            cmd   => 'numerics',
            args  => [ value => 'BOOL' ],
            query => QYES,
        },
        {
            cmd   => 'space',
            args  => [ space => ENUM( 'value', 'distance' ) ],
            query => QYES,
        },
        {
            cmd   => 'font',
            args  => [ font => ENUM( 'times', 'helvetica', 'courier' ), ],
            query => QYES,
        },
        {
            cmd   => 'fontsize',
            args  => [ size => POSINT ],
            query => QYES,
        },
        {
            cmd   => 'fontweight',
            args  => [ weight => ENUM( 'normal', 'bold' ) ],
            query => QYES,
        },

        {
            cmd   => 'fontslant',
            args  => [ slant => ENUM( 'roman', 'italic' ) ],
            query => QYES,
        },

        {
            cmd   => 'size',
            args  => [ size => POSINT ],
            query => QYES,
        },

        {
            cmd   => 'ticks',
            args  => [ ticks => POSINT ],
            query => QYES,
        },

        {
            cmd   => 'match',
            query => QYES
        },

        {
            cmd   => 'lock',
            args  => [ lock => BOOL ],
            query => QYES
        },

        {
            args  => [ value => BOOL ],
            query => QYES
        },

    ],

    #------------------------------------------------------

    console => [ {
            query => QNONE,
        }
    ],

    #------------------------------------------------------

    # contour []
    #    [yes|no]
    #    [<coordsys> [<skyframe>]]
    #    [clear]
    #    [generate]
    #    [load <filename>]
    #    [save <filename> [<coordsys> <skyframe>]]
    #    [convert]
    #    [load levels <filename>]
    #    [save levels <filename>]
    #    [copy]
    #    [paste [<coordsys> <color> <width> yes|no]]
    #    [color <color>]
    #    [width <width>]
    #    [dash yes|no]
    #    [smooth <smooth>]
    #    [method block|smooth]
    #    [nlevels <number of levels>]
    #    [scale linear|log|pow|squared|sqrt|asinh|sinh|histequ]
    #    [log exp <value>]
    #    [mode minmax|<value>|zscale|zmax]
    #    [scope global|local]
    #    [limits <min> <max>]
    #    [levels <value value value...>]
    #    [open|close]

    contour => [

        {
            args => [ value => BOOL ],
        },
        {
            args => [
                coordsys => COORDSYS,
                skyframe => SKYFRAME,
            ],
        },
        {
            cmd   => 'clear',
            query => QNONE,
        },

        {
            cmd   => 'generate',
            query => QNONE,
        },

        {
            cmd   => [ ENUM( 'load', 'save' ), 'levels' ],
            args  => [ filename => STRING ],
            query => QNONE,
        },

        {
            cmd  => 'load',
            args => [ filename => STRING ],
        },

        {
            cmd  => 'save',
            args => [ filename => STRING ],
        },

        {
            cmd  => 'save',
            args => [
                filename => STRING,
                coordsys => COORDSYS,
                skyframe => SKYFRAME,
            ],
        },

        {
            cmd   => 'copy',
            query => QNONE,
        },

        {
            cmd  => 'paste',
            args => [
                coordsys => COORDSYS,
                color    => COLOR,
                width    => POSINT,
                dash     => BOOL,
            ],
            query => QNONE
        },

        {
            cmd   => 'paste',
            query => QNONE
        },

        {
            cmd   => 'color',
            args  => [ color => COLOR ],
            query => QYES,
        },

        {
            cmd   => 'width',
            args  => [ width => POSINT ],
            query => QYES,
        },

        {
            cmd   => 'dash',
            args  => [ dash => BOOL ],
            query => QYES,
        },

        {
            cmd   => 'smooth',
            args  => [ smooth => POSINT ],
            query => QYES,
        },

        {
            cmd   => 'method',
            args  => [ method => ENUM( 'block', 'smooth' ) ],
            query => QYES,
        },

        {
            cmd   => 'nlevels',
            args  => [ nlevels => POSINT ],
            query => QYES,
        },

        {
            cmd  => 'scale',
            args => [
                scale => ENUM( 'linear', 'log', 'pow', 'squared', 'sqrt', 'asinh', 'sinh', 'histequ' ),
            ],
            query => QYES,
        },
        {
            cmd   => [ 'log', 'exp' ],
            args  => [ value => FLOAT ],
            query => QYES,
        },

        {
            cmd  => 'mode',
            args => [
                mode => ENUM( 'minmax', 'value', 'scale', 'zmax' )
            ],
            query => QYES,
        },

        {
            cmd   => 'mode',
            args  => [ mode => FLOAT ],
            query => QYES,
        },

        {
            cmd   => 'scope',
            args  => [ scope => ENUM( 'global', 'local' ), ],
            query => QYES,
        },

        {
            cmd  => 'limits',
            args => [
                min => FLOAT,
                max => FLOAT,
            ],
            query => QYES,
        },

        {
            cmd  => 'levels',
            args => [ levels => ARRAY ]
        },

        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE,
        },

    ],

    #------------------------------------------------------

    # crop [<x> <y> <width> <height> [<coordsys>][<skyframe>][degrees|arcmin|arcsec]
    #     [match <coordsys>]
    #     [lock <coordsys>|none]
    #     [reset]
    #     [3d zmin zmax <coordsys>]
    #     [open|close]


    crop => [

        {
            cmd   => 'match',
            args  => [ coordsys => COORDSYS ],
            query => QYES,
        },

        {
            cmd   => [ 'lock', 'none' ],
            query => QYES
        },

        {
            cmd   => 'lock',
            args  => [ coordsys => COORDSYS ],
            query => QYES
        },

        {
            cmd   => 'reset',
            query => QYES,
        },

        {
            cmd  => '3d',
            args => [
                zmin     => FLOAT,
                zmax     => FLOAT,
                coordsys => COORDSYS,
            ],
            query => QYES
        },

        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE,
        },

        {
            args => [
                x      => FLOAT,
                y      => FLOAT,
                width  => POSINT,
                height => POSINT,
            ],
            query => QYES,
        },

        {
            args => [
                x        => FLOAT,
                y        => FLOAT,
                width    => POSINT,
                height   => POSINT,
                coordsys => COORDSYS,
                skyframe => SKYFRAME,
                units    => ANGLE_UNIT,
            ],
            query => QNONE,
        },

        {
            query => QARGS | QONLY,
            args  => [
                coordsys => COORDSYS,
                skyframe => SKYFRAME,
                units    => ANGLE_UNIT,
            ],
            rvals => [
                x      => FLOAT,
                y      => FLOAT,
                width  => POSINT,
                height => POSINT,
            ],
        },

    ],


    #------------------------------------------------------

    # crosshair [<x> <y> <coordsys> [<skyframe>]]
    #     [match <coordsys>]
    #     [lock <coordsys>|none]

    crosshair => [
        # get physical coordinates
        {
            query => QYES,
            rvals => [ x => FLOAT, y => FLOAT ],
        },

        {
            args  => [ coordsys => COORDSYS ],
            query => QARGS | QYES,
            rvals => [ x => STRING, y => STRING ],
        },

        {
            args => [
                coordsys  => COORDSYS,
                skyformat => SKYFORMAT,
            ],
            query => QARGS | QYES,
            rvals => [ x => STRING, y => STRING ]
        },

        {
            args => [
                coordsys => COORDSYS,
                skyframe => SKYFRAME,
            ],
            query => QARGS | QYES,
            rvals => [ x => STRING, y => STRING ]
        },

        {
            args => [
                coordsys  => COORDSYS,
                skyframe  => SKYFRAME,
                skyformat => SKYFORMAT,
            ],
            query => QARGS | QYES,
            rvals => [ x => STRING, y => STRING ]
        },

        {
            args => [
                ra       => COORD_RA,
                dec      => COORD_DEC,
                coordsys => COORDSYS,
            ],
            query => QNONE,
        },

        {
            args => [
                ra       => COORD_RA,
                dec      => COORD_DEC,
                coordsys => COORDSYS,
                skyframe => SKYFRAME,
            ],
            query => QNONE,
        },
    ],

    #------------------------------------------------------

    # cube []
    #     [play|stop|next|prev|first|last]
    #     [<slice> [<coordsys>]]
    #     [interval <numeric>]
    #     [match <coordsys>]
    #     [lock <coordsys>|none]
    #     [order 123|132|213|231|312|321]
    #     [axes lock [yes|no]]
    #     [axis <axis>]
    #     [open|close]

    cube => [

        {
            query => QYES,
        },

        {
            cmd   => ENUM( 'play', 'stop', 'next', 'prev', 'first', 'last' ),
            query => QNONE,
        },

        {
            args  => [ slice => STRING, ],
            query => QNONE,
        },

        {
            args  => [ slice => STRING, coordsys => COORDSYS, ],
            query => QNONE,
        },

        {
            cmd   => 'interval',
            args  => [ interval => FLOAT ],
            query => QYES,
        },

        {
            cmd   => 'match',
            args  => [ coordsys => COORDSYS ],
            query => QYES,
        },

        {
            cmd   => [ 'lock', 'none' ],
            query => QYES
        },

        {
            cmd   => 'lock',
            args  => [ coordsys => COORDSYS ],
            query => QYES
        },


        {
            cmd  => 'order',
            args => [
                order => ENUM( '123', '132', '213', '231', '312', '321' )
            ],
        },

        {
            cmd   => [ 'axes', 'lock' ],
            args  => [ value => BOOL ],
            query => QYES,
        },

        {
            cmd   => 'axis',
            args  => [ axis => ENUM( '1', '2', '3' ) ],
            query => QYES,
        },

        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE,
        },

    ],


    #------------------------------------------------------
    # cursor [<x> <y>]

    cursor => [

        {
            args  => [ x => FLOAT, y => FLOAT ],
            query => QNONE,
        },
    ],

    #------------------------------------------------------

    # data [<coordsys> [<skyframe>] <x> <y> <width> <height> [yes|no]]

    data => [

        {
            args => [
                coordsys => COORDSYS,
                OPTIONAL( skyframe => SKYFRAME ),
                x      => STRING,
                y      => STRING,
                width  => FLOAT,
                height => FLOAT,
                OPTIONAL( stripcoords => BOOL ),
            ],
            query => QONLY | QARGS,
        },
    ],

    #------------------------------------------------------

    # envi [new] <header> [<filename>]

    envi => [

        {
            cmd  => 'new',
            args => [
                header   => STRING,
                filename => STRING,
            ],
            query => QNONE,
        },

        {
            cmd   => 'new',
            args  => [ header => STRING, ],
            query => QNONE,
        },

        {
            args => [
                header   => STRING,
                filename => STRING,
            ],
            query => QNONE,
        },

        {
            args  => [ header => STRING, ],
            query => QNONE,
        }
    ],

    #------------------------------------------------------

    # exit
    # quit

    ENUM( 'exit', 'quit' ) => [

        { query => QNONE }
    ],


    #------------------------------------------------------

    # export[array|nrrd|envi|gif|tiff|jpeg|png]<filename>
    # export array <filename>[big|little|native]
    # export nrrd <filename>[big|little|native]
    # export envi <header> [<filename>][big|little|native]
    # export jpeg <filename>[1-100]
    # export tiff <filename>[none|jpeg|packbits|deflate]


    export => [ {
            cmd  => 'array',
            args => [
                filename => STRING,
                OPTIONAL( endian => ENDIAN ),
            ],
        },
        {
            cmd  => 'nrrd',
            args => [
                filename => STRING,
                OPTIONAL( endian => ENDIAN ),
            ],
        },
        {
            cmd  => 'envi',
            args => [
                header => STRING,
                OPTIONAL( filename => STRING ),
                OPTIONAL( endian   => ENDIAN ),
            ],
        },

        {
            cmd  => 'jpeg',
            args => [
                filename => STRING,
                OPTIONAL( quality => POSINT ),
            ],
        },


        {
            cmd  => 'tiff',
            args => [
                filename => STRING,
                OPTIONAL(
                    compression => ENUM( 'none', 'jpeg', 'packbits', 'deflate' )
                ),
            ],
        },

    ],

    #------------------------------------------------------

    # file

    file => [ {
            query => QONLY,
        }
    ],


    #------------------------------------------------------


    # file => [

    #       [
    #           [ ENUM( 'fits', 'mosaic', 'mosaicimage' ) ],
    #           {
    #               args  => [STRING],
    #               attrs => [ new => BOOL ],
    #               query => QNONE
    #           }
    #       ],

    #       [
    #           ['array'],
    #           {
    #               args  => [STRING],
    #               attrs => [
    #                   new    => BOOL,
    #                   bitpix => INT,
    #                   skip   => INT,
    #                   -o     => [
    #                       [ -a  => [ xdim => FLOAT, ydim => FLOAT ] ],
    #                       [ dim => FLOAT ]
    #                   ],
    #               ],
    #               query => QNONE,
    #           }
    #       ],

    #       [
    #           ['url'],
    #           {
    #               args  => [STRING],
    #               attrs => [ new => BOOL ],
    #               query => QNONE,
    #           }
    #       ],

    #       [
    #           ['save'],
    #           {
    #               args  => [STRING],
    #               query => QNONE,
    #           }
    #       ],

    #       [
    #           [ 'save', 'gz' ],
    #           {
    #               args  => [STRING],
    #               query => QNONE,
    #           }
    #       ],

    #       [
    #           [ 'save', 'resample' ],
    #           {
    #               args  => [STRING],
    #               query => QNONE,
    #           }
    #       ],

    #       [
    #           [ 'save', 'resample', 'gz' ],
    #           {
    #               args  => [STRING],
    #               query => QNONE,
    #           }
    #       ],

    #       [
    #           [],
    #           {
    #               args  => [STRING],
    #               attrs => [
    #                   new     => BOOL,
    #                   extname => STRING,
    #                   filter  => STRING_STRIP,
    #                   bin     => ARRAY( 1, 2 ),
    #               ],
    #           }
    #       ],

    # ],

    #------------------------------------------------------

    # fits[new|mask|slice] [<filename>]
    #    [width|height|depth|bitpix]
    #    [size [wcs|wcsa...wcsz] [fk4|fk5|icrs|galactic|ecliptic] [degrees|arcmin|arcsecs]]
    #    [header [<ext>] [keyword <string>]]
    #    [image|table|slice]

    fits => [

        {
            query     => QONLY,
            bufferarg => 1,
        },

        {
            cmd  => ENUM( 'width', 'height', ' depth' ),
            args => [ value => POSINT ],
        },

        {
            cmd  => 'bitpix',
            args => [ value => INT ],
        },

        {
            cmd  => 'size',
            args => [
                value => POSINT,
                OPTIONAL(
                    wcs      => WCS_STR,
                    skyframe => SKYFRAME,
                    units    => ANGLE_UNIT,
                ),
            ],
        },

        {
            cmd  => 'header',
            args => [
                extension => POSINT,
                OPTIONAL(
                    wcs      => WCS_STR,
                    skyframe => SKYFRAME,
                    units    => ANGLE_UNIT,
                ),
            ],
        },


        # [
        #     ['mosaic'],
        #     {
        #         args  => [SCALARREF],
        #         attrs => [
        #             new     => BOOL,
        #             extname => STRING,
        #             filter  => STRING_STRIP,
        #             bin     => ARRAY( 1, 2 ),
        #         ],
        #         query  => QNONE,
        #         bufarg => 1,
        #         cvt    => 0,
        #         retref => 1,
        #         chomp  => 0,
        #     }
        # ],

        # [
        #     ['mosaicimage'],
        #     {
        #         args  => [SCALARREF],
        #         attrs => [
        #             new     => BOOL,
        #             extname => STRING,
        #             filter  => STRING_STRIP,
        #             bin     => ARRAY( 1, 2 ),
        #         ],
        #         query  => QNONE,
        #         bufarg => 1,
        #         cvt    => 0,
        #         retref => 1,
        #         chomp  => 0,
        #     }
        # ],

        # [ ['type'], { query => QONLY } ],

        # [
        #     [ 'image', 'gz' ],
        #     {
        #         query  => QONLY,
        #         cvt    => 0,
        #         rvals  => [STRING],
        #         retref => 1,
        #         chomp  => 0,
        #     }
        # ],

        # [
        #     ['image'],
        #     {
        #         query  => QONLY,
        #         cvt    => 0,
        #         rvals  => [STRING],
        #         retref => 1,
        #         chomp  => 0,
        #     }
        # ],

        # [
        #     [ 'resample', 'gz' ],
        #     {
        #         query  => QONLY,
        #         cvt    => 0,
        #         rvals  => [STRING],
        #         retref => 1,
        #         chomp  => 0,
        #     }
        # ],

        # [
        #     ['resample'],
        #     {
        #         query  => QONLY,
        #         cvt    => 0,
        #         rvals  => [STRING],
        #         retref => 1,
        #         chomp  => 0,
        #     }
        # ],

        # [
        #     [],
        #     {
        #         args  => [SCALARREF],
        #         attrs => [
        #             new     => BOOL,
        #             extname => STRING,
        #             filter  => STRING_STRIP,
        #             bin     => ARRAY( 1, 2 ),
        #         ],
        #         query  => QYES,
        #         bufarg => 1,
        #         cvt    => 0,
        #         retref => 1,
        #         chomp  => 0,
        #     }
        # ],

    ],

    #------------------------------------------------------

    frame => [

        [
            ['all'],
            {
                query  => QONLY,
                rvals  => [ARRAY],
                retref => 1
            }
        ],

        [ ['first'], { query => QNONE } ],

        [ ['next'], { query => QNONE } ],

        [ ['prev'], { query => QNONE } ],

        [ ['last'], { query => QNONE } ],

        [ [ 'new', 'rgb' ], { query => QNONE } ],


        [ ['new'], { query => QNONE } ],

        [
            ['center'],
            { query => QNONE },
            {
                args  => [INT],
                query => QNONE
            },
            {
                args  => [ ENUM( 'all' ) ],
                query => QNONE
            },
        ],

        [
            ['clear'],
            { query => QNONE },
            {
                args  => [INT],
                query => QNONE
            },
            {
                args  => [ ENUM( 'all' ) ],
                query => QNONE
            }
        ],

        [
            ['delete'],
            { query => QNONE },
            {
                args  => [INT],
                query => QNONE
            },
            {
                args  => [ ENUM( 'all' ) ],
                query => QNONE
            }
        ],

        [
            ['reset'],
            { query => QNONE },
            {
                args  => [INT],
                query => QNONE
            },
            {
                args  => [ ENUM( 'all' ) ],
                query => QNONE
            }
        ],

        [
            ['refresh'],
            { query => QNONE },
            {
                args  => [INT],
                query => QNONE
            },
            {
                args  => [ ENUM( 'all' ) ],
                query => QNONE
            }
        ],

        [
            ['hide'],
            { query => QNONE },
            {
                args  => [INT],
                query => QNONE
            },
            {
                args  => [ ENUM( 'all' ) ],
                query => QNONE
            }
        ],

        [
            ['show'],
            { query => QNONE },
            {
                args  => [INT],
                query => QNONE
            },
            {
                args  => [ ENUM( 'all' ) ],
                query => QNONE
            }

        ],

        [
            ['move'],
            {
                args  => [ ENUM( 'first', 'back', 'forward', 'last' ) ],
                query => QNONE
            }
        ],

        [ ['frameno'], { args => [INT] }, ],

        [ [], { args => [INT] } ],

    ],


    #------------------------------------------------------

    grid => [

        [
            ['load'],
            {
                args  => [STRING],
                query => QNONE
            },
        ],

        [
            ['save'],
            {
                args  => [STRING],
                query => QNONE
            },
        ],

        [ [], { args => [BOOL] } ],

    ],

    #------------------------------------------------------

    height => [ [ [], { args => [INT] } ] ],


    #------------------------------------------------------

    iconify => [ [ [], { args => [BOOL] } ], ],

    #------------------------------------------------------

    lower => [ [ [], { query => QNONE } ], ],


    #------------------------------------------------------

    minmax => [

        [
            ['mode'],
            {
                args => [ ENUM( 'scan', 'sample', 'datamin', 'irafmin' ) ] }
        ],

        [ ['interval'], { args => [INT] } ],

        [
            [],
            {
                args => [ ENUM( 'scan', 'sample', 'datamin', 'irafmin' ) ] }
        ],

    ],

    #------------------------------------------------------

    mode => [ [
            [],
            {
                args => [ ENUM( qw< none region crosshair colorbar pan zoom rotate catalog examine > ) ],
            }
        ],
    ],

    #------------------------------------------------------

    nameserver => [

        [
            ['name'],
            {
                args  => [STRING],
                query => QNONE
            }
        ],

        [
            ['server'],
            {
                args => [ ENUM( 'ned-sao', 'ned-eso', 'simbad-sao', 'simbad-eso' ) ]
            },
        ],

        [ ['skyformat'], { args => [SKYFORMAT] } ],

        [ ['close'], { query => QNONE } ],

        [ ['open'], { query => QNONE } ],

        [
            [],
            {
                args  => [STRING],
                query => QNONE
            }
        ],

    ],

    #------------------------------------------------------

    orient => [ [ [], { args => [ ENUM( 'none', 'x', 'y', 'xy' ) ] }, ], ],

    #------------------------------------------------------

    page => [

        [ [ 'setup', 'orientation' ], { args => [ ENUM( 'portrait', 'landscape' ) ], } ],

        [ [ 'setup', 'pagescale' ], { args => [ ENUM( 'scaled', 'fixed' ) ], } ],

        [
            [ 'setup', 'pagesize' ],
            {
                args => [ ENUM( 'letter', 'legal', 'tabloid', 'poster', 'a4' ) ],
            }
        ],

    ],


    #------------------------------------------------------

    pan => [

        [
            ['to'],
            {
                args  => [ COORD_RA, COORD_DEC ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFORMAT ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME, SKYFORMAT ],
                query => QNONE
            },
        ],

        [
            [ REWRITE( 'abs', 'to' ) ],
            {
                args  => [ COORD_RA, COORD_DEC ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFORMAT ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME, SKYFORMAT ],
                query => QNONE
            },
        ],

        [
            [ EPHEMERAL( 'rel' ) ],
            {
                args  => [ COORD_RA, COORD_DEC ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFORMAT ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME, SKYFORMAT ],
                query => QNONE
            },
        ],

        [
            [],
            {
                args  => [ COORD_RA, COORD_DEC ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFORMAT ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME ],
                query => QNONE
            },
            {
                args  => [ COORD_RA, COORD_DEC, COORDSYS, SKYFRAME, SKYFORMAT ],
                query => QNONE
            },

            {
                rvals => [ STRING, STRING ],
                cvt   => 0
            },

            {
                args  => [ COORDSYS, SKYFORMAT ],
                query => QONLY,
                rvals => [ STRING, STRING ],
                cvt   => 0
            },

            {
                args  => [ COORDSYS, SKYFRAME, SKYFORMAT ],
                query => QONLY,
                rvals => [ STRING, STRING ],
                cvt   => 0
            },

            {
                args  => [ COORDSYS, SKYFRAME ],
                query => QONLY,
                rvals => [ STRING, STRING ],
                cvt   => 0
            },

            {
                args  => [COORDSYS],
                query => QONLY,
                rvals => [ STRING, STRING ],
                cvt   => 0
            },


        ],

    ],

    #------------------------------------------------------

    pixeltable =>
      [ [ [], { args => [ ENUM( 'yes', 'no' ) ] }, { args => [ ENUM( 'open', 'close' ) ] }, ], ],

    #------------------------------------------------------

    print => [

        [ ['destination'], { args => [ ENUM( 'printer', 'file' ) ] }, ],

        [ ['command'], { args => [STRING] }, ],

        [ ['filename'], { args => [STRING] }, ],

        [ ['palette'], { args => [ ENUM( 'rgb', 'cmyk', 'gray' ) ] }, ],

        [ ['level'], { args => [ ENUM( '1', '2' ) ] }, ],

        [ ['resolution'], { args => [ ENUM( qw( 53 72 75 150 300 600  ) ) ] }, ],

        [ [], { query => QNONE } ],

    ],


    #------------------------------------------------------

    quit => [ [ [], { query => QNONE }, ] ],


    #------------------------------------------------------

    raise => [ [ [], { query => QNONE }, ] ],


    #------------------------------------------------------

    regions => [

        [ [ ENUM( qw( movefront moveback selectall selectnone deleteall ) ) ], { query => QNONE }, ],


        [
            [ ENUM( qw( load save ) ) ],
            {
                args  => [STRING],
                query => QNONE
            },
        ],

        [ ['format'], { args => [REGIONFORMAT] }, ],

        [ ['system'], { args => [COORDSYS] }, ],

        [ ['sky'], { args => [SKYFRAME] }, ],

        [ ['skyformat'], { args => [SKYFORMAT] }, ],

        [ ['strip'], { args => [BOOL] }, ],

        [
            [ ENUM( qw(source background include exclude selected) ) ],
            {
                query => QONLY
            }
        ],

        [ ['shape'], { args => [STRING] } ],

        [ ['width'], { args => [INT] } ],

        [
            ['color'],
            {
                args => [ ENUM( qw( black white red green blue cyan magenta yellow) ) ] }
        ],


        [
            [],
            {
                args   => [STRING_NL],
                query  => QNONE,
                bufarg => 1,
            },
            {
                query => QYES | QONLY | QATTR,
                rvals => [STRING],
                attrs => [
                    -format    => REGIONFORMAT,
                    -system    => COORDSYS,
                    -sky       => SKYFRAME,
                    -skyformat => SKYFORMAT,
                    -strip     => BOOL,
                    -prop      => ENUM(
                        qw( select edit move rotate delete fixed
                          include source )
                    ),
                ] }
        ],

    ],

    #------------------------------------------------------

    rotate => [

        [
            ['to'],
            {
                args  => [FLOAT],
                query => QNONE
            },
        ],

        [
            [ REWRITE( 'abs', 'to' ) ],
            {
                args  => [FLOAT],
                query => QNONE
            },
        ],

        [
            [ EPHEMERAL( 'rel' ) ],
            {
                args  => [FLOAT],
                query => QNONE
            },
        ],


        [ [], { args => [FLOAT] }, ],

    ],

    #------------------------------------------------------

    saveas => [ [
            [ ENUM( qw( jpeg tiff png ppm ) ) ],
            {
                args  => [STRING],
                query => QNONE
            },
        ]
    ],

    #------------------------------------------------------

    scale => [

        [ ['datasec'], { args => [BOOL] }, ],

        [ ['limits'], { args => [ FLOAT, FLOAT ] }, ],

        [ ['mode'], { args => [ ENUM( qw( minmax zscale zmax ) ) ] }, { args => [FLOAT] }, ],

        [ ['scope'], { args => [ ENUM( qw( local global ) ) ] }, ],

        [
            [],
            {
                args => [ ENUM( qw( linear log squared sqrt histequ ) ) ] }
        ],

    ],

    #------------------------------------------------------

    single => [ [
            [ EPHEMERAL( 'state' ) ],
            {
                rvals => [BOOL],
                query => QONLY
            },
        ],

        [ [], { query => QNONE }, ]
    ],

    #------------------------------------------------------

    smooth => [

        [
            ['function'],
            {
                args => [ ENUM( 'boxcar', 'tophat', 'gaussian' ) ]
            },
        ],

        [ ['radius'], { args => [FLOAT] }, ],

        [ [], { args => [BOOL] }, ],

    ],


    #------------------------------------------------------

    source => [ [
            [],
            {
                args  => [STRING],
                query => QNONE
            },
        ],
    ],

    #------------------------------------------------------

    tcl => [ [
            [],
            {
                args  => [STRING],
                query => QNONE
            },
        ],
    ],

    #------------------------------------------------------

    tile => [
        [ ['mode'], { args => [ ENUM( 'grid', 'column', 'row' ) ] } ],

        [ [ 'grid', 'mode' ], { args => [ ENUM( 'automatic', 'manual' ) ] }, ],

        [ [ 'grid', 'layout' ], { args => [ INT, INT ] }, ],

        [ [ 'grid', 'gap' ], { args => [INT] }, ],

        [ [ ENUM( 'grid', 'row', 'column' ) ], { query => QNONE }, ],

        [
            [ EPHEMERAL( 'state' ) ],
            {
                rvals => [BOOL],
                query => QONLY
            },
        ],

        [ [], { args => [BOOL] } ],
    ],

    #------------------------------------------------------

    update => [
        [],
        {
            attrs => [ now => BOOL ],
            query => QNONE
        },
        {
            args  => [ INT, FLOAT, FLOAT, FLOAT, FLOAT ],
            attrs => [ now => BOOL ],
            query => QNONE
        }
    ],

    #------------------------------------------------------

    version => [ [
            [],
            {
                rvals => [STRING],
                query => QONLY
            },
        ],
    ],

    #------------------------------------------------------

    view => [ [ [
                ENUM(
                    qw( info
                      panner
                      magnifier
                      buttons
                      filename
                      object
                      minmax
                      lowhigh
                      frame
                      red
                      green
                      blue
                    ) )
            ],
            { args => [BOOL] },
        ],

        [ ['layout'], { args => [ ENUM( 'vertical', 'horizontal' ) ] }, ],


        [ [ 'colorbar', 'numerics' ], { args => [BOOL] }, ],


        [ ['colorbar'], { args => [ ENUM( 'vertical', 'horizontal' ) ] }, { args => [BOOL] }, ],

        [ [ 'graph', 'vertical' ], { args => [BOOL] }, ],

        [ [ 'graph', 'horizontal' ], { args => [BOOL] }, ],

        [ [COORDSYS], { args => [BOOL] }, ]
    ],

    #------------------------------------------------------

    vo => [ [ [], { args => [STRING] } ], ],

    #------------------------------------------------------

    wcs => [

        [ ['system'], { args => [WCS_STR] } ],

        [ ['sky'], { args => [SKYFRAME] } ],

        [ ['skyformat'], { args => [SKYFORMAT] } ],

        [ ['align'], { args => [BOOL] } ],

        [ ['reset'], { query => QNONE }, ],


        [
            [ 'replace', 'file' ],
            {
                args  => [STRING],
                query => QNONE
            },
        ],

        [
            [ 'append', 'file' ],
            {
                args  => [STRING],
                query => QNONE
            },
        ],

        [
            [ ENUM( 'replace', 'append' ) ],
            {
                args   => [WCS_SCALARREF],
                query  => QNONE,
                bufarg => 1
            },
            {
                args   => [WCS_HASH],
                query  => QNONE,
                bufarg => 1
            },
            {
                args   => [WCS_ARRAY],
                query  => QNONE,
                bufarg => 1
            },
        ],

        [ [], { args => [WCS_STR] }, ],

    ],

    #------------------------------------------------------

    web => [ [ [], { args => [STRING] } ] ],


    #------------------------------------------------------

    width => [ [ [], { args => [INT] } ] ],


    #------------------------------------------------------

    zoom => [ [
            ['to'],
            {
                args  => [FLOAT],
                query => QNONE
            },
            {
                args  => ['fit'],
                query => QNONE
            },
        ],

        [
            [ REWRITE( 'abs' => 'to' ) ],
            {
                args  => [FLOAT],
                query => QNONE
            },
        ],

        [
            [ EPHEMERAL( 'rel' ) ],
            {
                args  => [FLOAT],
                query => QNONE
            },
        ],

        [ [ REWRITE( '0' => 'to fit' ) ], { query => QNONE }, ],

        [ [ REWRITE( tofit => 'to fit' ) ], { query => QNONE }, ],

        [ [], { args => [FLOAT] } ],

    ],
);


#------------------------------------------------------

# base:
#     [<object>]
#     [name <object>|clear]
#     [<ra> <dec>] # in wcs fk5
#     [size <width> <height> degrees|arcmin|arcsec]
#     [save yes|no]
#     [frame new|current]
#     [update frame|crosshair]
#     [open|close]

# 2mass []
#     [survey j|h|k]

# dsssao []

# dsseso []
#     [survey DSS1|DSS2-red|DSS2-blue|DSS2-infrared]

# dssstsci []
#     [survey poss2ukstu_red|poss2ukstu_ir|poss2ukstu_blue]
#     [survey poss1_blue|poss1_red]
#     [survey all|quickv|phase2_gsc2|phase2_gsc1]



for my $survey ( qw[ 2mass dsssao dsseso dssstsci ] ) {

    $Grammar{$survey} = [

        # name clear
        {
            cmd   => [ 'name', 'clear' ],
            query => QNONE,
        },

        # name <object>
        {
            cmd   => 'name',
            args  => [ name => STRING ],
            query => QYES,
        },

        # coord
        {
            cmd   => 'coord',
            args  => [ ra => COORD_RA, dec => COORD_DEC, ],
            query => QONLY,
        },

        # size <width> <height> degrees|arcmin|arcsec
        {
            cmd  => 'size',
            args => [
                width  => POSNUM,
                height => POSNUM,
                unit   => ANGLE_UNIT,
            ],
            query => QYES,
        },

        # save yes|no
        {
            cmd  => 'save',
            args => [
                value => BOOL
            ],
            query => QYES,
        },

        # frame new|current
        {
            cmd  => 'frame',
            args => [
                destination => ENUM( 'new', 'current' ),
            ],
            query => QYES,
        },

        # update frame|crosshair
        {
            cmd  => 'update',
            args => [
                value => ENUM( 'frame', 'crosshair' ),
            ],
            query => QNONE,
        },

        #  survey, if available
        {

            '2mass' => {
                cmd  => 'survey',
                args => [
                    value => ENUM( 'h', 'j', 'k' ),
                ],
                query => QYES,
            },

            dsseso => {
                cmd  => 'survey',
                args => [
                    survey => ENUM( 'DSS1', 'DSS2-red', 'DSS2-blue', 'DSS2-infrared' )
                ],
                query => QYES,
            },

            dssstsci => {
                cmd  => 'survey',
                args => [
                    survey => ENUM(
                        'poss2ukstu_red', 'poss2ukstu_ir', 'poss2ukstu_blue', 'poss1_blue',
                        'poss1_red',      'all',           'quickv',          'phase2_gsc2',
                        'phase2_gsc1',
                    )
                ],
                query => QYES,
            }

        }->{$survey} // (),

        # open|close
        {
            cmd   => ENUM( 'open', 'close' ),
            query => QNONE,
        },

        #  <ra> <dec> # in wcs fk5
        {
            args => [
                ra  => COORD_RA,
                dec => COORD_DEC,
            ],
            query => QNONE,
        },

        #  <object>
        {
            args  => [ object => STRING ],
            query => QNONE,
        },

        # []
        {
            query => QNONE,
        }

      ],
      ;

}

sub grammar {
    return \%Grammar;
}

# COPYRIGHT

1;
