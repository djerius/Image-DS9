package Image::DS9::Util;

# ABSTRACT: Various useful things

use v5.10;
use strict;
use warnings;

our $VERSION = 'v1.0.1';

use SemVer;
use Safe::Isa;
use Exporter 'import';

our @EXPORT_OK = qw( parse_version TODO is_TODO );

sub parse_version {
    my $string = shift;
    my ( $base, $sfx ) = $string =~ /(?:\S+\s+)?((?:\d+[.]?)+)([^.\d]+.*)?/;
    my $version = join( q{-}, $base, ( $sfx // () ) );
    return SemVer->declare( $version );
}

sub TODO {
    # use an anonymous array so it duck type's as an empty command
    return bless [], 'Image::DS9::Grammar::TODO';
}

sub is_TODO {
    return $_[0]->$_isa( 'Image::DS9::Grammar::TODO' );
}

# COPYRIGHT

1;

=pod

=for Pod::Coverage
parse_version

=cut
